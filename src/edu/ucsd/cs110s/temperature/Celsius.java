package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO: Complete this method
		return (this.getValue() + " degrees");
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Temperature temp2 = new Celsius(getValue());
		return temp2;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float temp = getValue();
		temp = temp*9/5 + 32;
		Temperature temp2 = new Celsius(temp);
		return temp2;
	}
}
