package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO: Complete this method
		return (this.getValue() + " degrees");
	}
	@Override
	public Temperature toCelsius() {
		float temp = getValue();
		temp = (temp-32)*5/9;
		Temperature temp2 = new Fahrenheit(temp);
		return temp2;

	}
	@Override
	public Temperature toFahrenheit() {
		Temperature temp2 = new Fahrenheit(getValue());
		return temp2;
	}
}
